#ifndef __HEALTH_TASK__
#define __HEALTH_TASK__

#include "Task.h"
#include "Message.h"

class SightTask: public Task {

public:
  SightTask();
  void init(int period);  
  void tick();
  byte rightEyeDistance;
  byte leftEyeDistance;
  
};

#endif
