#include "IrReceiver.h"
#include "Arduino.h"

IrReceiver::IrReceiver(int pin){
  this->pin = pin;
  pinMode(pin,INPUT);
}


bool IrReceiver::receive(){
  return digitalRead(pin) == HIGH;  
};
