#ifndef __CONNECTION_TASK__
#define __CONNECTION_TASK__

#include "Task.h"
#include "SoftwareSerial.h"
#include "MsgService.h"

class ConnectionTask: public Task {

public:
  ConnectionTask();
  void init(int period);  
  void tick();
};

#endif
