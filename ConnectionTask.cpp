#include "ConnectionTask.h"
#include "config.h"
#include "MsgService.h"
#include "Message.h"

SoftwareSerial btChannel(0, 1);
byte counter = 0;

ConnectionTask::ConnectionTask(){

}

void ConnectionTask::init(int period){
  btChannel.begin(9600);
  Task::init(period);
}
  
void ConnectionTask::tick(){

  Message* m = Message::getInstance();

  if (Serial.available() > 0) {
    byte b = Serial.read();
    Serial.write(b);
    if (b == 'H') {
      m->gameUp = true;
    } else if (b=='B') {
      m->gameUp = false;
    }
  }

  if (m->gameUp) {
    if (m->hit) {
      Serial.write("DEAD.");
    } else {
      Serial.write("ALIVE.");      
    }  
  }
  
}
