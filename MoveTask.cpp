#include "MoveTask.h"
#include "config.h"
#include "MsgService.h"
#include "SoftwareSerial.h"
#include "Motor.h"
#include "ConnectionTask.h"
#include "Message.h"

#define enA 9
#define in1 4
#define in2 5
#define enB 10
#define in3 6
#define in4 7

Motor rightMotor(in1,in2,enA);
Motor leftMotor(in3,in4,enB);

MoveTask::MoveTask(){

}
  
void MoveTask::init(int period){
  Task::init(period);  
  m = Message::getInstance();
  leftMotor.setBackwards(false);
  rightMotor.setBackwards(false);
  rightMotor.startMotor();
  leftMotor.startMotor();
}
  
void MoveTask::tick(){

  if (m->gameUp) {
  
    if (m->newMessageM) {
  
      int rightSpeed = (255 - m->rightSpeed);
      if (rightSpeed > 255) {
        rightSpeed = 255;
      }   
      int leftSpeed = (255 - m->leftSpeed);
      if (leftSpeed > 255) {
        leftSpeed = 255;
      }   
  
      rightMotor.setSpeed(rightSpeed);
      leftMotor.setSpeed(leftSpeed);
    
      m->newMessageM = false; 
      
    }
  }

}
