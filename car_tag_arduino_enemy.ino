#include "Scheduler.h"
#include "MsgService.h"
#include "MoveTask.h"
#include "ConnectionTask.h"
#include "SightTask.h"
#include "HealthTask.h"

Scheduler sched;

void setup() {

  sched.init(25);

  MsgService.init();

  SightTask *pSightTask = new SightTask();
  pSightTask->init(25);
  sched.addTask(pSightTask); 

  MoveTask* pMoveTask = new MoveTask();
  pMoveTask->init(25);
  sched.addTask(pMoveTask);

  ConnectionTask* pConTask = new ConnectionTask();
  pConTask->init(25);
  sched.addTask(pConTask);

  HealthTask* pHealthTask = new HealthTask();
  pHealthTask -> init(25);
  sched.addTask(pHealthTask);

}

void loop() {
  sched.schedule();
}
