#ifndef __MOTOR__
#define __MOTOR__

class Motor { 
public:
  Motor(int pinIn1, int pinIn2, int pinEn);
  void setSpeed(int speed);
  void setBackwards(bool backwards);
  void startMotor();
private:

  int pinIn1; //used for direction and motor enabling
  int pinIn2; //used for direction and motor enabling

  int pinEn; //used for speed
  bool backwards;
  int speed;
  void writeDirection();
};

#endif
