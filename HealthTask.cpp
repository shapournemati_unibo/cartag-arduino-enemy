#include "HealthTask.h"
#include "config.h"
#include "Message.h"
#include "IrReceiver.h"

#define irReceiverPin 12

IrReceiver irReceiver(irReceiverPin);

HealthTask::HealthTask(){

}
  
void HealthTask::init(int period){
  Task::init(period);
}
  
void HealthTask::tick(){

  Message* m = Message::getInstance();
  if (m->gameUp) {
    m->hit = irReceiver.receive();
    m->newMessageH = true;  
  }

}
