#include "Arduino.h"

#ifndef __MESSAGE__
#define __MESSAGE__

class Message {
  private:  
    static Message* instance;
    Message();
  public:
    static Message* getInstance();
    byte leftSpeed;
    byte rightSpeed;
    bool hit;
    bool newMessageM;
    bool newMessageS;
    bool newMessageH;
    bool gameUp;
};

#endif
