#include "Motor.h"
#include "Arduino.h"

Motor::Motor(int pinIn1, int pinIn2, int pinEn) {
  this->pinIn1 = pinIn1;
  this->pinIn2 = pinIn2;
  this->pinEn = pinEn;
  pinMode(pinIn1,OUTPUT);
  pinMode(pinIn2,OUTPUT);
  pinMode(pinEn,OUTPUT);
}

void Motor::setSpeed(int speed) {
  this->speed = speed;
  analogWrite(pinEn, speed);
}

void Motor::setBackwards(bool backwards) {
  this->backwards = backwards;
  writeDirection();  
}

void Motor::writeDirection() {
  if (backwards) {
    // Set backwards pins
    digitalWrite(pinIn1, HIGH);
    digitalWrite(pinIn2, LOW);
  } else {
    // Set farward pins
    digitalWrite(pinIn1, LOW);
    digitalWrite(pinIn2, HIGH);
  }
}

void Motor::startMotor() {
  analogWrite(pinEn, speed);
  writeDirection();
}


