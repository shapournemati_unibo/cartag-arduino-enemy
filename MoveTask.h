#ifndef __MOVE_TASK__
#define __MOVE_TASK__

#include "Task.h"
#include "Motor.h"
#include "Message.h"

class MoveTask: public Task {

public:
  MoveTask();
  void init(int period);  
  void tick();

private:
  Message* m;
};

#endif

