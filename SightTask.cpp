#include "SightTask.h"
#include "config.h"
#include "Message.h"
#include "Sonar.h"

#define echo1 2
#define trig1 3
#define echo2 8
#define trig2 11

Sonar s1(echo1,trig1);
Sonar s2(echo2,trig2);

float d1, d2;

SightTask::SightTask(){

}
  
void SightTask::init(int period){
  Task::init(period);
  rightEyeDistance = 255;
  leftEyeDistance = 255;
}
  
void SightTask::tick(){
  Message* m = Message::getInstance();
  if (m->gameUp) {
    d1 = s1.getDistance() * 200;
    d2 = s2.getDistance() * 200;
    if (d1 > 255) {
      d1 = 255;
    }
    if (d2 > 255) {
      d2 = 255;
    }
    m->rightSpeed = d1;
    m->leftSpeed = d2;
    m->newMessageM = true;
  }
}
