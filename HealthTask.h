#ifndef __SIGHT_TASK__
#define __SIGHT_TASK__

#include "Task.h"
#include "Message.h"

class HealthTask: public Task {

public:
  HealthTask();
  void init(int period);  
  void tick();  
};

#endif
