#ifndef __IR_RECEIVER__
#define __IR_RECEIVER__

#include "IrReceiver.h"

class IrReceiver { 
public:
  IrReceiver(int pin);
  bool receive();    
protected:
  int pin;
};

#endif
