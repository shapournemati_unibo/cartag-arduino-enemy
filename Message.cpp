#include"Message.h"

Message* Message::instance = 0;

Message* Message::getInstance() {
  if (instance == 0) {
    instance = new Message();
    instance -> leftSpeed = 2;
    instance -> rightSpeed = 2;
    instance -> hit = false;
    instance -> newMessageS = false;
    instance -> newMessageM = false;
    instance -> newMessageH = false;
    instance -> gameUp = false;
  }
  return instance;
}

Message::Message() {}
